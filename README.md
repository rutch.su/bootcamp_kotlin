### Running the CorDapp

* Build a test network of nodes by opening a terminal window at the root of
  your project and running the following command:

    * Windows:   `gradlew.bat deployNodes` or `gradlew.bat clean deployNodes`
    * macOS:     `./gradlew clean deployNodes`


* Start the nodes by running the following command:

    * Windows:   `call build/nodes/runnodes.bat`
    * macOS:     `build/nodes/runnodes`

* Open the nodes are started, go to the terminal of Party A (not the notary!)
  and run the following command to issue 99 tokens to Party B:

   `flow start TokenIssueFlow amount: 99, owner: “O=PartyB,L=New York,C=US”`


* You can now see the tokens in the vaults of Party A and Party B (but not 
  Party C!) by running the following command in their respective terminals:

    `run vaultQuery contractStateType: com.template.states.TokenState`



* Reference 
    [Docs corda](https://docs.r3.com/en/platform/corda/4.5/open-source/tutorial-cordapp.html) 
