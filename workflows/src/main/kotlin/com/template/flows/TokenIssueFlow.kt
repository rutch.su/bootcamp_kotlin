package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import com.template.contracts.TokenContract
import com.template.contracts.TokenContract.Companion.IOU_CONTRACT_ID
import com.template.states.TokenState
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder


@InitiatingFlow
@StartableByRPC
class TokenIssueFlow(val amount : Int, val owner: Party ) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
        val notary = serviceHub.networkMapCache.notaryIdentities.single()
        val tokenState = TokenState(serviceHub.myInfo.legalIdentities.first(),owner,amount)
        val issueCommand = Command(TokenContract.Commands.Issue(), tokenState.participants.map { it.owningKey })

        val builder = TransactionBuilder(notary = notary)
        builder.addOutputState(tokenState,IOU_CONTRACT_ID)
        builder.addCommand(issueCommand)
        builder.verify(serviceHub)

        val ptx = serviceHub.signInitialTransaction(builder)

        val session = (tokenState.participants - ourIdentity).map{initiateFlow(it)}.toSet()

        val stx = subFlow(CollectSignaturesFlow(ptx, session))

        return subFlow(FinalityFlow(stx, session))
    }
}


@InitiatedBy(TokenIssueFlow::class)
class TokenIssueFlowResponder(val flowSession: FlowSession) : FlowLogic<SignedTransaction>(){

    @Suspendable
    override fun call(): SignedTransaction {
        val signedTransactionFlow = object : SignTransactionFlow(flowSession) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                val output = stx.tx.outputs.single().data
                "This must be an IOU transaction" using (output is TokenState)
            }
        }

        val txWeJustSignedId = subFlow(signedTransactionFlow)
        return subFlow(ReceiveFinalityFlow(otherSideSession = flowSession, expectedTxId = txWeJustSignedId.id))
    }

}