package com.template.states

import com.template.contracts.TokenContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party



@BelongsToContract(TokenContract::class)
data class TokenState(val issuer : Party,
                      val owner : Party,
                      val amount : Int): ContractState {

    override val participants: List<AbstractParty> get() = listOf(issuer,owner)


}