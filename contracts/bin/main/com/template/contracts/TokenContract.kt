package com.template.contracts

import com.template.states.TokenState
import net.corda.core.contracts.*
import net.corda.core.transactions.LedgerTransaction

class TokenContract : Contract {

    companion object{
        @JvmStatic
        val IOU_CONTRACT_ID = TokenContract::class.java.name
    }

    interface Commands : CommandData{
        class Issue : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction) {

        val command = tx.commands.requireSingleCommand<Commands>()
        when(command.value){
            is Commands.Issue -> requireThat{
                "No inputs should be consumed when issuing an IOU." using (tx.inputs.isEmpty())
                "Only one output state should be created when issuing an IOU." using (tx.outputs.size == 1)

                val output = tx.outputsOfType<TokenState>().single()

                "A newly issued IOU must have a positive amount." using (output.amount > 0 )
                "The lender and borrower cannot have the same identity." using (output.issuer != output.owner)
                "Both lender and borrower together only may sign IOU issue transaction." using
                        (command.signers.toSet() == output.participants.map{it.owningKey}.toSet())
            }
        }
    }
}